/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.chayanon.oxprogram.Player;
import com.chayanon.oxprogram.Table;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author W I N 1 0
 */
public class testTable {

    public testTable() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testRow1ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

    @Test
    public void testRow2ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

    @Test
    public void testRow3ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

    @Test
    public void testCol1ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

    @Test
    public void testCol2ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

    @Test
    public void testCol3ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

    @Test
    public void testRow1ByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(O, X);
        table.setRowCol(0, 0);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }

    @Test
    public void testRow2ByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(O, X);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }

    @Test
    public void testRow3ByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(O, X);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }

    @Test
    public void testCol1ByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(O, X);
        table.setRowCol(0, 0);
        table.setRowCol(1, 0);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }

    @Test
    public void testCol2ByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(O, X);
        table.setRowCol(0, 1);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }

    @Test
    public void testCol3ByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(O, X);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }

    @Test
    public void testDiagonal1ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

    @Test
    public void testDiagonal2ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }
    
    @Test
    public void testDiagonal1ByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(O, X);
        table.setRowCol(0, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }

    @Test
    public void testDiagonal2ByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(O, X);
        table.setRowCol(0, 2);
        table.setRowCol(1, 1);
        table.setRowCol(2, 0);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(O, table.getWinner());
    }
    
    @Test
    public void testDraw(){
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 1);
        table.setRowCol(1, 0);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.switchPlayer();
        table.setRowCol(0, 0);
        table.setRowCol(0, 2);
        table.setRowCol(1, 2);
        table.setRowCol(2, 0);
        table.setRowCol(2, 1);
        table.checkWin();
        assertEquals(true, table.isFinish());
        assertEquals(null, table.getWinner());
    }
    

}
